from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hola Mundo!'

@app.route('/saludos')
def saluda():
    return 'Saludos!!'

@app.route('/params/')
@app.route('/params/<name>')
@app.route('/params/<name>/<int:num>')
def params(name = 'Este es un valor de Default', num = 'nada'):
    return 'El parametro es : {}, {}'.format(name, num)

if __name__ == '__main__':
    app.run( debug=True ,port=8000 )