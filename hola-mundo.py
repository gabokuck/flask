from flask import Flask

app = Flask(__name__)#Nuevo objeto

@app.route('/')#wrap o decorador indicar al servidor las rutas

def index():#Función para regresar un string
    return 'Hola Mundo!'

app.run()#Ejecutar el servidor